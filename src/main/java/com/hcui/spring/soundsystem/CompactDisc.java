package com.hcui.spring.soundsystem;

/**
 * Created by hcui on 11/6/16.
 */
public interface CompactDisc {
    void play();
}
