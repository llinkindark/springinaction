package com.hcui.spring.soundsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by hcui on 11/6/16.
 */
@Component
public class CDPlayer implements  MediaPlayer{

    private final CompactDisc cd;

    @Autowired
    public CDPlayer(CompactDisc disc){
        this.cd = disc;
    }

    @Override
    public void play() {
        cd.play();
    }
}
