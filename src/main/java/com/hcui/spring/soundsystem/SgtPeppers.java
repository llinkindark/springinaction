package com.hcui.spring.soundsystem;

import org.springframework.stereotype.Component;

/**
 * Created by hcui on 11/6/16.
 */
@Component("IAmHappy")
public class SgtPeppers implements CompactDisc {
    private final String TITLE = "Sgt. Pepper's Lonely Hearts Club Band";
    private final String ARTIST = "The Beatles";
    @Override
    public void play(){
        System.out.println(String.format("Playing %s by %s", TITLE, ARTIST));
    }
}
